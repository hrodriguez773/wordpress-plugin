<?php
/**
 * Class visitPanama
 *
 * Plugin Name: Visit Panama Api
 * Plugin URI: http://atp.rootstacl.net
 * Description: This is the atp rest api plugin.
 * Version: 1.0.0
 * Author: Rootstack
 *
 */

require_once plugin_dir_path(__FILE__).'class/ATP_Rest_Controller.php';

class visitPanama
{
    function __construct()
    {
        add_action('rest_api_init', array('ATP_Rest_Controller', 'register_routes'));
    }

    function activate()
    {
        add_action('mail_init', array('ATP_Rest_Controller', 'init_mail'));
    }
}

$visit_panama = new visitPanama();
register_activation_hook(__FILE__, array($visit_panama, 'activate'));