<?php

require_once plugin_dir_path(dirname(__FILE__)).'endpoints/provinceEndpoint.php';
require_once plugin_dir_path(dirname(__FILE__)).'endpoints/eventEndpoints.php';
require_once plugin_dir_path(dirname(__FILE__)).'endpoints/activitieEndpoints.php';
require_once plugin_dir_path(dirname(__FILE__)).'endpoints/homeEndpoints.php';
require_once plugin_dir_path(dirname(__FILE__)).'endpoints/destinationEndpoints.php';
require_once plugin_dir_path(dirname(__FILE__)).'endpoints/tourOperatorEndpoints.php';
require_once plugin_dir_path(dirname(__FILE__)).'endpoints/lodgingEndpoints.php';
require_once plugin_dir_path(dirname(__FILE__)).'endpoints/travelEndpoints.php';
require_once plugin_dir_path(dirname(__FILE__)).'endpoints/contactFormEndpoints.php';
require_once plugin_dir_path(dirname(__FILE__)).'endpoints/instagramEndpoints.php';

class ATP_Rest_Controller
{
    private $_jwt_plugin = 'jwt-auth';
    private $_jwt_version = '1.1.0';

    public function register_routes()
    {
        $atp_controller = new ATP_Rest_Controller();

        // attractions
        register_rest_route(
            'atp/v1',
            '/attractions',
            array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array('destinationEndpoints', 'get_attractions'),
                'permission_callback' => array($atp_controller, 'validate_user'),
                'args' => array(
                    'tag' => null,
                    'count' => null
                )
            )
        );

        register_rest_route(
            'atp/v1',
            '/get_attraction/(?P<id>\d+)',
            array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array('destinationEndpoints', 'get_attraction'),
                'permission_callback' => array($atp_controller, 'validate_user'),
                'args' => array(
                    'tag' => null,
                    'count' => null
                )
            )
        );

        //activities
        register_rest_route(
            'atp/v1',
            '/activities',
            array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array('activitieEndpoints', 'get_activities'),
                'permission_callback' => array($atp_controller, 'validate_user'),
                'args' => array(
                    'tag' => null,
                    'count' => null
                )
            )
        );

        register_rest_route(
            'atp/v1',
            '/activitie_category',
            array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array('activitieEndpoints', 'get_activitie_category'),
                'permission_callback' => array($atp_controller, 'validate_user'),
                'args' => array(
                    'place' => null,
                    'activity_type' => null,
                )
            )
        );

        register_rest_route(
            'atp/v1',
            '/activities_profile',
            array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array('activitieEndpoints', 'activities_info'),
                'permission_callback' => array($atp_controller, 'validate_user')
            )
        );

        register_rest_route(
            'atp/v1',
            '/activitie/(?P<id>\d+)',
            array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array('activitieEndpoints', 'get_activitie'),
                'permission_callback' => array($atp_controller, 'validate_user'),
                'args' => array(
                    'tag' => null,
                    'count' => null
                )
            )
        );

        //Events
        register_rest_route(
            'atp/v1',
            '/events',
            array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array('eventEndpoints', 'get_events'),
                'permission_callback' => array($atp_controller, 'validate_user')
            )
        );

        register_rest_route(
            'atp/v1',
            '/current_event',
            array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array('eventEndpoints', 'get_current_event'),
                'permission_callback' => array($atp_controller, 'validate_user'),
                'args' => array(
                    'month',
                    'year'
                )
            )
        );

        register_rest_route(
            'atp/v1',
            '/event/(?P<id>\d+)',
            array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array('eventEndpoints', 'get_event'),
                'permission_callback' => array($atp_controller, 'validate_user')
            )
        );

        //home
        register_rest_route(
            'atp/v1',
            '/home',
            array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array('homeEndpoints', 'home_info'),
                'permission_callback' => array($atp_controller, 'validate_user')
            )
        );

        register_rest_route(
            'atp/v1',
            '/lodgings',
            array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array('lodgingEndpoints', 'get_lodgings'),
                'permission_callback' => array($atp_controller, 'validate_user'),
                'args' => array(
                    'province_term' => null,
                )
            )
        );

        //provinces
        register_rest_route(
            'atp/v1',
            '/provinces',
            array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array('provinceEndpoint', 'get_provinces'),
                'permission_callback' => array($atp_controller, 'validate_user'),
                'args' => array(
                    'tag' => null,
                    'count' => null,
                    'language' => 'es'
                )
            )
        );

        register_rest_route(
            'atp/v1',
            '/province/(?P<id>\d+)',
            array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array('provinceEndpoint', 'get_province'),
                'permission_callback' => array($atp_controller, 'validate_user'),
                'args' => array(
                    'tag' => null,
                    'count' => null
                )
            )
        );

        //tour operators
        register_rest_route(
            'atp/v1',
            '/tour_operators',
            array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array('tourOperatorEndpoints', 'get_tour_operators'),
                'permission_callback' => array($atp_controller, 'validate_user'),
                'args' => array(
                    'province_term' => null
                )
            )
        );

        //travel
        register_rest_route(
            'atp/v1',
            '/travel',
            array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array('travelEndpoints', 'get_travel'),
                'permission_callback' => array($atp_controller, 'validate_user')
            )
        );

        //contact form
        register_rest_route(
            'atp/v1',
            '/submit_form',
            array(
                'methods' => WP_REST_Server::CREATABLE,
                'callback' => array('contactFormEndpoints', 'post_form'),
                'permission_callback' => array($atp_controller, 'validate_user'),
                'args' => array(
                    'nombre' => null,
                    'correo' => null,
                    'pais' => null,
                    'comentario' => null,
                )
            )
        );

        //instagram
        register_rest_route(
            'atp/v1',
            '/instagram/images',
            array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array('instagramEndpoints', 'get_instagram_feed_images'),
                'permission_callback' => array($atp_controller, 'validate_user'),
                'args' => array(
                    'tag' => null,
                    'count' => null
                )
            )
        );
    }

    public function validate_user($request = null)
    {
        $jwt = new Jwt_Auth_Public($this->_jwt_plugin, $this->_jwt_version);
        $response = $jwt->validate_token(true);
        if (is_wp_error($response)) {
            return false;
        }
        return true;
    }
}
