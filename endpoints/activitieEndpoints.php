<?php

class activitieEndpoints extends WP_REST_Controller
{
    public function get_activitie_category($request = null)
    {
        $activitieEndpoint = new activitieEndpoints();
        $category_query_args = array(
            'post_type' => 'destino',
            'posts_per_page' => -1,
            'post_status' => 'publish',
            'category__in' => array($request['place'], $request['activity_type'] )
        );
        $posts = get_posts($category_query_args);
        if ($request['language'] !== 'en') {
            $response = $activitieEndpoint->format_response_meta($posts, $request['tag'], $request['count']);

            return ["activities" => $response];
        }

        $response = $activitieEndpoint->format_response_meta_english($posts);

        return ["activities" => $response];
    }

    public function get_activities($request = null)
    {
        $activitieEndpoint = new activitieEndpoints();
        $category_query_args = array(
            'post_type' => 'actividad',
            'category_name' => '',
            'posts_per_page'=>'-1',
            'post_status' => 'publish'
        );
        $posts = get_posts($category_query_args);
        if ($request['language'] !== 'en') {
            $response = $activitieEndpoint->format_response_meta($posts, $request['tag'], $request['count']);

            return ["activities" => $response];
        }

        $response = $activitieEndpoint->format_response_meta_english($posts);

        return ["activities" => $response];
    }

    public function get_activitie($request = null)
    {
        $ig_endpoints = new InstagramEndpoints();
        $activitieEndpoint = new activitieEndpoints();
        $post = get_post($request['id']);
        $meta = get_post_meta($post->ID);
        $thumbnail = get_the_post_thumbnail_url($post->ID);
        $link = get_permalink($post->ID);
        $location = $activitieEndpoint->format_url($meta['location'][0]);

        $response = ["id" => $post->ID,
                     "title" => $post->post_title,
                     "thumbnail" => $thumbnail,
                     "post_type" => $post->post_type,
                     "arrive" => $meta['como_llegar'][0],
                     "weather" => $meta['clima'][0],
                     "useful_info" => wp_strip_all_tags($meta['informacion_util'][0], true),
                     "contacts" => wp_strip_all_tags($meta['contactos'][0], true),
                     "tourist_facilities" => wp_strip_all_tags($meta['facilidades_turisticas'][0], true),
                     "description" => wp_strip_all_tags($meta['descripcion'][0], true),
                     "ig_images" => $ig_endpoints->get_instagram_feed_images($request['tag'], $request['count']),
                     "link" => $link,
                     "video" => $meta['video'] === null ? [] : $meta['video'],
                     "location" => $meta['location'][0] === null ? '' : $meta['location'],
                     "tourist_facilities" => wp_strip_all_tags($meta['facilidades_turisticas'][0], true),
                     "useful_info" => wp_strip_all_tags($meta['informacion_util'][0], true),
                     "latitude" => $location[1][0] === null ? '' : $location[1][0],
                     "longitude" => $location[2][0] === null ? '' : $location[2][0]
        ];

        return $response;
    }

    public function activities_info($request)
    {
        $activitieEndpoint = new activitieEndpoints();
        $args = [
            'post_type' => 'page',
            'category_name' => 'perfil_actividad'
        ];
        $image_container_args = [
            'post_type' => 'fl-builder-template',
            'fl-builder-template-category' => 'actividades_iconos',
        ];
        $image_args = [
            'post_type' => 'attachment',
            'post_parent'=> null,
            'mime_type' => 'image'
        ];
        $posts = get_posts($args);
        if ($request['language'] !== 'en') {
            $activities_image_container = get_posts($image_container_args);
            $image_args['post_parent'] = $activities_image_container[0]->ID;
            $image_post = get_posts($image_args);
            $post_response = $activitieEndpoint->format_response($posts);
            $image_response = $activitieEndpoint->format_activities_info($image_post, $request['tag'], $request['count']);

            return ["activities_posts" => $post_response , "home_post_info" => $image_response];
        }

        $activities_image_container = get_posts($image_container_args);
        $image_args['post_parent'] = $activities_image_container[0]->ID;
        $image_post = get_posts($image_args);
        $post_response = $activitieEndpoint->format_response_english($posts);
        $image_response = $activitieEndpoint->format_activities_info($image_post, $request['tag'], $request['count']);

        return ["activities_posts" => $post_response , "home_post_info" => $image_response];
    }

    public function format_activities_info($result = null, $tag = null, $count = null)
    {
        $ig_endpoints = new InstagramEndpoints();
        $response = [];
        foreach ($result as $element) {
            $post_meta = get_post_meta($element->ID);
            $link = get_permalink($element->ID);
            $response[] = [
                "id" => $element->ID,
                "title" => $element->post_title,
                "post_name" => $element->post_name,
                "image" => $element->guid,
                "link" => $link,
                "video" => $post_meta['video'] === null ? [] : $post_meta['video']
            ];
        }
        return $response;
    }

    public function format_response($result = null, $tag = null, $count = null)
    {
        $ig_endpoints = new InstagramEndpoints();
        $response = [];
        $image_args = [
            'post_parent' => null,
            'post_type' => 'attachment',
            'post_mime_type' => 'image/jpeg',
        ];
        $ig_images = $ig_endpoints->get_instagram_feed_images($request['tag'], $request['count']);

        foreach ($result as $element) {
            $image_array = [];
            $image_args['post_parent'] = $element->ID;
            $images = get_posts($image_args);
            $thumbnail = get_the_post_thumbnail_url($element->ID);
            $meta = get_post_meta($element->ID);
            if (!empty($images)) {
                foreach ($images as $item) {
                    $image_array []= ["id" => $item->ID, "title" => $item->post_title, "post_name" => $item->post_name,
                                      "image" => $item->guid];
                }
            }
            $content = preg_replace("/\r|\n/", "", $meta['activitie_description']);
            $terms = get_term_by('slug', $element->post_name, 'category');
            $link = get_permalink($element->ID);

            $response[] = [
                "id" => $element->ID,
                "title" => $element->post_title,
                "post_name" => $element->post_name,
                "thumbnail" => $thumbnail,
                "term_id" => $terms->term_id,
                "post_type" => $element->post_type,
                "content" => $content,
                "ig_images" => $ig_images,
                "link" => $link,
                "video" => $meta['video'] === null ? [] : $meta['video']
            ];
        }

        return $response;
    }

    public function format_response_english($result = null, $tag = null, $count = null)
    {
        $ig_endpoints = new InstagramEndpoints();
        global $wpdb;
        $response = [];
        $image_args = [
            'post_parent' => null,
            'post_type' => 'attachment',
            'post_mime_type' => 'image/jpeg',
        ];
        $ig_images = $ig_endpoints->get_instagram_feed_images($request['tag'], $request['count']);

        if ($result !== null) {
            $query = "SELECT element_id from wp_icl_translations where language_code = 'en' AND trid = ";
            foreach ($result as $elements) {
                $query_result[] = $wpdb->get_results($query.$elements->ID);
            }
            if (!empty($query_result)) {
                $ig_images = $ig_endpoints->get_instagram_feed_images($tag, $count);
                foreach ($query_result as $element) {
                    if (!empty($element)) {
                        $query = get_post($element[0]->element_id);
                        $image_array = [];
                        $image_args['post_parent'] = $query->ID;
                        $images = get_posts($image_args);
                        $thumbnail = get_the_post_thumbnail_url($query->ID);
                        $meta = get_post_meta($query->ID);
                        if (!empty($images)) {
                            foreach ($images as $item) {
                                $image_array []= ["id" => $item->ID, "title" => $item->post_title, "post_name" => $item->post_name,
                                                  "image" => $item->guid];
                            }
                        }
                        $content = preg_replace("/\r|\n/", "", $meta['activitie_description']);
                        $terms = get_term_by('slug', $query->post_name, 'category');
                        $link = get_permalink($query->ID);

                        $response[] = [
                            "id" => $query->ID,
                            "title" => $query->post_title,
                            "post_name" => $query->post_name,
                            "thumbnail" => $thumbnail,
                            "term_id" => $terms->term_id,
                            "post_type" => $query->post_type,
                            "content" => $content,
                            "ig_images" => $ig_images,
                            "link" => $link,
                            "video" => $meta['video'] === null ? [] : $meta['video']
                        ];
                    }
                }
            }
        }

        return $response;
    }

    public function format_response_meta_english($result = null, $tag = null, $count = null)
    {
        global $wpdb;
        $ig_endpoints = new InstagramEndpoints();
        $activitieEndpoint = new activitieEndpoints();
        $response = [];
        $images = [];
        $images_args = array(
            'post_type' => 'attachment',
            'post_parent'=> null,
            'mime_type' => 'image'
        );

        if ($result !== null) {
            $query = "SELECT element_id from wp_icl_translations where language_code = 'en' AND trid = ";
            foreach ($result as $elements) {
                $query_result[] = $wpdb->get_results($query.$elements->ID);
            }
            if (!empty($query_result)) {
                $ig_images = $ig_endpoints->get_instagram_feed_images($tag, $count);
                foreach ($query_result as $element) {
                    if (!empty($element)) {
                        $query = get_post($element[0]->element_id);
                        $thumbnail = get_the_post_thumbnail_url($query->ID);
                        $post_meta = get_post_meta($query->ID);
                        $link = get_permalink($query->ID);
                        $location = $activitieEndpoint->format_url($post_meta['location'][0]);
                        $images["image"] = $thumbnail;

                        $response[] = [
                            "id" => $query->ID,
                            "title" => $query->post_title ? null : '',
                            "post_name" => $query->post_name ? null : '',
                            "post_type" => $query->post_type ? null : '',
                            "image" => $images ? null : '',
                            "thumbnail" => $thumbnail ? null : '',
                            "arrive" => $post_meta['como_llegar'][0] ? null : '',
                            "weather" => $post_meta['clima'][0] ? null : '',
                            "useful_info" => wp_strip_all_tags($post_meta['informacion_util'][0], true),
                            "contacts" => wp_strip_all_tags($post_meta['contactos'][0], true),
                            "tourist_facilities" => wp_strip_all_tags($post_meta['facilidades_turisticas'][0], true),
                            "description" => wp_strip_all_tags($post_meta['descripcion'][0], true),
                            "link" => $link,
                            "ig_images" => $ig_images,
                            "location" => $post_meta['location'][0] === null ? '' : $post_meta['location'],
                            "latitude" => $location[1][0] === null ? '' : $location[1][0],
                            "longitude" => $location[2][0] === null ? '' : $location[2][0]
                        ];
                    }
                }
            }
        }

        return $response;
    }

    public function format_response_meta($result = null, $tag = null, $count = null)
    {
        $ig_endpoints = new InstagramEndpoints();
        $activitieEndpoint = new activitieEndpoints();
        $response = [];
        $images = [];
        $images_args = array(
            'post_type' => 'attachment',
            'post_parent'=> null,
            'mime_type' => 'image'
        );

        $ig_images = $ig_endpoints->get_instagram_feed_images($request['tag'], $request['count']);

        foreach ($result as $element) {
            $images_args['post_parent'] = $element->ID;
            $image_query = get_posts($images_args);
            $post_meta = get_post_meta($element->ID);
            $link = get_permalink($element->ID);
            $location = $activitieEndpoint->format_url($post_meta['location'][0]);
            $thumbnail = get_the_post_thumbnail_url($element->ID);

            $images["image"] = $thumbnail;

            $response[] = [
                "id" => $element->ID,
                "title" => $element->post_title,
                "post_name" => $element->post_name,
                "post_type" => $element->post_type,
                "image" => $images,
                "thumbnail" => $thumbnail,
                "arrive" => $post_meta['como_llegar'][0],
                "weather" => $post_meta['clima'][0],
                "useful_info" => wp_strip_all_tags($post_meta['informacion_util'][0], true),
                "contacts" => wp_strip_all_tags($post_meta['contactos'][0], true),
                "tourist_facilities" => wp_strip_all_tags($post_meta['facilidades_turisticas'][0], true),
                "description" => wp_strip_all_tags($post_meta['descripcion'][0], true),
                "link" => $link,
                "ig_images" => $ig_images,
                "location" => $meta['location'][0] === null ? '' : $meta['location'],
                "tourist_facilities" => wp_strip_all_tags($meta['facilidades_turisticas'][0], true),
                "useful_info" => wp_strip_all_tags($meta['informacion_util'][0], true),
                "latitude" => $location[1][0] === null ? '' : $location[1][0],
                "longitude" => $location[2][0] === null ? '' : $location[2][0]
            ];
        }
        return $response;
    }

    public function format_url($url = null)
    {
        $re = '/@(\-?[0-9]+\.[0-9]+),(\-?[0-9]+\.[0-9]+)/';
        preg_match($re, $url, $matches, PREG_OFFSET_CAPTURE, 0);
        // Print the entire match result
        return $matches;
    }
}
