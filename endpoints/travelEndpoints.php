<?php

class travelEndpoints
{
    public function get_travel($request = null)
    {
        $travel_args = [
            "post_type" => "viaje"
        ];
        $travel_post = new WP_Query($travel_args);

        foreach ($travel_post->get_posts() as $item) {
            $url = get_page_link($item->ID);
        }

        return ["page_url" => $url];
    }
}