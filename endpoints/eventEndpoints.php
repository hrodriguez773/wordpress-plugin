<?php

global $eventon;

class eventEndpoints extends WP_REST_Controller
{
    function get_events($request = null)
    {
        $eventEndpoint = new eventEndpoints();
        $args = [
            "post_type" => 'ajde_events',
            "posts_per_page" => -1,
            "order" => "asc",
            "orderby" => "evcal_start_date"
        ];
        $event = get_posts($args);
        $response = $eventEndpoint->format_event_list($event);
        $count = count($response);
        return ["events" => $response, "count" => $count];
    }

    public function get_event($request = null)
    {
        $eventEndpoint = new eventEndpoints();
        $post_id = $request['id'];
        $event[] = get_post($post_id);
        $response = $eventEndpoint->format_event_list($event);
        return $response;
    }

    public function get_current_event($request = null)
    {
        $eventEndpoint = new eventEndpoints();
        $args = [
            "post_type" => 'ajde_events',
            "posts_per_page" => -1,
            "order" => "asc",
            "orderby" => "evcal_start_date"
        ];
        $requested_month = $request['month'];
        $requested_year = $request['year'];
        $events = get_posts($args);
        $response = [];
        if (count($events) > 0) {
            $response = $eventEndpoint->format_month_events($events, $requested_month, $requested_year);
        }
        if (count($events) === 0) {
            $event_aux [] = $events;
            $response = $eventEndpoint->format_month_events($event_aux, $requested_month, $requested_year);
        }
        return $response;
    }

    public function format_month_events($event = null, $requested_month = null, $requested_year = null)
    {
        $result = [];
        $event_image_args = [
            'post_type' => 'attachment',
            'post_parent'=> null,
            'mime_type' => 'image'
        ];
        foreach ($event as $element) {
            $post_id = $element->ID;
            $meta = get_post_meta($post_id);
            $event_start = $meta['evcal_srow'][0];
            $event_end = $meta['evcal_erow'][0];
            if (date('Y', $event_start) === $requested_year && date('n', $event_start) === $requested_month) {
                $event_image_args['post_parent'] = $post_id;
                $image_query = get_posts($event_image_args);
                if (!empty($image_query)) {
                    foreach ($image_query as $_item) {
                        $image = $_item->guid;
                    }
                }
                $term = wp_get_post_terms($post_id, 'event_location');
                $event_location = $term[0]->name;
                $result[] = ["id" => $element->ID, "title" => $element->post_title, "initial" => $event_start,
                    "description" => wp_strip_all_tags($meta['_evcal_ec_f1a1_cus'][0], true),
                    "end" => $event_end, "location" => $event_location, "event_image" => $image];
            }
        }
        return $result;
    }

    public function format_event_list($event = null)
    {
        $result = [];
        $event_image_args = [
            'post_type' => 'attachment',
            'post_parent'=> null,
            'mime_type' => 'image'
        ];
        foreach ($event as $element) {
            $post_id = $element->ID;
            $meta = get_post_meta($post_id);
            $event_start = $meta['evcal_srow'][0];
            $event_end = $meta['evcal_erow'][0];
            $event_image_args['post_parent'] = $post_id;
            $image_query = get_posts($event_image_args);
            if (!empty($image_query)) {
                foreach ($image_query as $_item) {
                    $image = $_item->guid;
                }
            }
            $term = wp_get_post_terms($post_id, 'event_location');
            $event_location = $term[0]->name;
            $result[] = [
                "id" => $element->ID,
                "title" => $element->post_title,
                "initial" => $event_start,
                "end" => $event_end,
                "description" => wp_strip_all_tags($meta['_evcal_ec_f1a1_cus'][0], true),
                "location" => $event_location,
                "event_image" => $image
            ];
        }
        return $result;
    }
}