<?php

class InstagramEndpoints
{
    public static function rudr_instagram_api_curl_connect( $api_url ) 
    {
        $connection = curl_init();
        curl_setopt($connection, CURLOPT_URL, $api_url);
        curl_setopt($connection, CURLOPT_RETURNTRANSFER, 1); // return the result, do not print
        curl_setopt($connection, CURLOPT_TIMEOUT, 20);
        $json_return = curl_exec($connection); // connect and get json data
        curl_close($connection); // close connection

        return json_decode($json_return); // decode and return
    }

    public static function get_instagram_feed_images($tag = null, $count = null)
    {
        $tags = $tag;
        $count = $request['count'];
        $access_token = '295059037.3a81a9f.c3de2561c759490c822759befc72cf7a';
        $base_url = 'https://api.instagram.com/v1/users/self/media/recent?access_token=' . $access_token;

        if (!is_null($count) && intval($count) > 0) {
            $base_url .= '&count=' . intval($count); 
        }

        $posts = InstagramEndpoints::rudr_instagram_api_curl_connect($base_url);

        foreach ($posts->data as $post) {
            if (!is_null($tags) && in_array($tags, $post->tags)) {
                $images[] = $post->images->low_resolution->url;
            }

            if (is_null($tags)) {
                $images[] = $post->images->low_resolution->url;
            }
        }

        return $images;
    }
}
