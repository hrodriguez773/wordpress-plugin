<?php

class lodgingEndpoints extends WP_REST_Controller
{
    public function get_lodgings($request = null)
    {
        $lodging_args = [
            "post_type" => "hospedaje",
            "category_name" => $request['province_term']
        ];
        $lodging_post = new WP_Query($lodging_args);
        foreach ($lodging_post->get_posts() as $item) {
            $url = get_page_link($item->ID);
        }
        return ["page_url" => $url];
    }
}