<?php

class tourOperatorEndpoints extends WP_REST_Controller
{
    public function get_tour_operators($request = null)
    {
        $agency_args = [
            "post_type" => "agencia",
            "category_name" => $request['province_term']
        ];
        $agency_post = new WP_Query($agency_args);
        foreach ($agency_post->get_posts() as $item) {
            $url = get_page_link($item->ID);
        }
        return ["page_url" => $url];
    }

}