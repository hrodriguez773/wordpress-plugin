<?php

class homeEndpoints
{
    public function home_info($request = null)
    {
        $homeEndpoint = new homeEndpoints();
        $parent_args = [
            'post_type' => 'page',
            'category_name' => 'pagina_inicio'
        ];
        $args = [
            'post_parent' => null,
            'post_type' => 'attachment',
            'post_mime_type' => 'image/jpeg',
        ];

        $form = GFFormsModel::get_form_meta(13);
        foreach ($form['fields'] as $value) {
            if ($value->adminLabel === 'pais') {
                $country_field = $value;
            }
        }
        $parent_post = get_posts($parent_args);
        $args['post_parent'] = $parent_post[0]->ID;
        $array_result = [];
        $result = get_posts($args);
        foreach ($result as $element) {
            $array_result [] = $element;
        }
        $response = $homeEndpoint->format_response($result, $parent_post, $country_field->choices);
        return $response;
    }

    public function format_response($result = null, $parent_post = null, $country_field = null) {
        $response = [];
        foreach ($result as $element) {
            $response [] = ["id" => $element->ID ,"image" => $element->guid, "title" => $element->post_title];
        }
        foreach ($parent_post as $_element) {
            $meta = get_post_meta($_element->ID);
            $publicitary_image = $meta['publicitary_image'];
        }
        $header_images = $meta['header_image'];
        foreach ($header_images as $image) {
            $header_array[] = ["image" => $image];
        }
        foreach ($publicitary_image as $pubimage) {
            $imageArray []= ["image" => $pubimage];
        }
        return [
            "result" => $header_array,
            "publicity" => $imageArray,
            "countries" => $country_field
        ];
    }
}