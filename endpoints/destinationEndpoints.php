<?php

class destinationEndpoints
{
    public function get_attractions($request = null)
    {
        $destinationEndpoints = new destinationEndpoints();
        $args = [
            'post_type' => 'destino',
            'posts_per_page' => '-1',
            'post_status' => 'publish',
            'suppress_filters' => false,
            "ICL_LANGUAGE_CODE" => "en",
        ];

        $posts = get_posts($args);
        if ($request['language'] !== 'en') {
            $response = $destinationEndpoints->format_response($posts, $request['tag'], $request['count']);
            return $response;
        }

        $response = $destinationEndpoints->get_english_destination($posts);

        return $response;
    }

    public function get_english_destination($posts = null, $tag = null, $count = null)
    {
        global $wpdb;
        $response = [];
        $ig_endpoints = new InstagramEndpoints();
        $destinationEndpoints = new destinationEndpoints();
        $args = [
            'post_type' => 'attachment',
            'post_mime_type' => 'image',
        ];
        if ($posts !== null) {
            $query = "SELECT element_id from wp_icl_translations where language_code = 'en' AND trid = ";
            foreach ($posts as $elements) {
                $query_result[] = $wpdb->get_results($query.$elements->ID);
            }
            if (!empty($query_result)) {
                $ig_images = $ig_endpoints->get_instagram_feed_images($tag, $count);
                foreach ($query_result as $element) {
                    if (!empty($element)) {
                        $query = get_post($element[0]->element_id);
                        $thumbnail = get_the_post_thumbnail_url($query->ID);
                        $meta = get_post_meta($query->ID);
                        $link = get_permalink($query->ID);
                        $location = $destinationEndpoints->format_url($meta['location'][0]);
                        $response[] = ["id" => $query->ID,"title" => $query->post_title, "post_type" => $query->post_type ,
                                       "thumbnail" => $thumbnail,
                                       "post_name" => $query->post_name, "arrive" => $destinationEndpoints->extract_url($meta['como_llegar'][0]),
                                       "description" => wp_strip_all_tags($meta['descripcion'][0], true),
                                       "to_do" => wp_strip_all_tags($meta['que_hacer'][0], true),
                                       "best_season" => wp_strip_all_tags($meta['mejor_temporada'][0], true),
                                       "iframe" => $meta['como_llegar'][0],
                                       "ig_images" => $ig_images,
                                       "weather" => $meta['clima'][0],
                                       "link" => $link,
                                       "video" => $meta['video'],
                                       "location" => $meta['location'][0] === null ? '' : $meta['location'],
                                       "tourist_facilities" => wp_strip_all_tags($meta['facilidades_turisticas'][0], true),
                                       "useful_info" => wp_strip_all_tags($meta['informacion_util'][0], true),
                                       "latitude" => $location[1][0] === null ? '' : $location[1][0],
                                       "longitude" => $location[2][0] === null ? '' : $location[2][0],
                        ];
                    }
                }
            }
        }
        return $response;
    }

    public function get_attraction($request = null)
    {
        $destinationEndpoints = new destinationEndpoints();
        $image_array = [];
        $image_args = [
            'post_parent' => null,
            'post_type' => 'attachment',
            'post_mime_type' => 'image/jpeg',
        ];
        $image_args['post_parent'] = $request['id'];
        $post[] = get_post($request['id']);
        $images = get_posts($image_args);
        if (empty($images)) {
            foreach ($images as $element) {
                $image_array[] = ["id" => $element->ID, 'image' => $element->guid];
            }
        }
        $response = $destinationEndpoints->format_response($post, $request['tag'], $request['count']);
        return $response;
    }

    public function format_response($result = null, $tag = null, $count = null)
    {
        $ig_endpoints = new InstagramEndpoints();
        $destinationEndpoints = new destinationEndpoints();
        $response = [];
        $term_regex = '/^provincia-/';
        $image_args = [
            'post_parent' => null,
            'post_type' => 'attachment',
            'post_mime_type' => 'image/jpeg',
        ];
        if (!empty($result)) {
            $ig_images = $ig_endpoints->get_instagram_feed_images($tag, $count);
            foreach ($result as $element) {
                $image_array = [];
                $image_args['post_parent'] = $element->ID;
                $meta = get_post_meta($element->ID);
                $image = get_posts($image_args);
                $thumbnail = get_the_post_thumbnail_url($element->ID);
                $parent_province = '';
                $link = get_permalink($element->ID);

                if (!empty($image)) {
                    foreach ($image as $item) {
                        $image_array[] = ["id" => $item->ID, 'image' => $item->guid];
                    }
                }
                $term = wp_get_post_terms($element->ID, 'category', array("fields" => "all"));
                foreach ($term as $_element) {
                    if (preg_match($term_regex, $_element->name, $matches, PREG_OFFSET_CAPTURE, 0)) {
                        if (!empty($matches[0])) {
                            $pieces = explode("-", $_element->name);
                            $parent_province = $pieces[1];
                        }
                    }
                }
                if ($meta['video'] === null) {
                    $meta['video'] = [];
                }
                $duplicate = wpml_get_post_duplicates_filter($element->ID);
                $location = $destinationEndpoints->format_url($meta['location'][0]);
                $response[] = ["id" => $element->ID,"title" => $element->post_title, "post_type" => $element->post_type ,
                               "thumbnail" => $thumbnail, "image" => $image_array,
                               "post_name" => $element->post_name, "arrive" => $destinationEndpoints->extract_url($meta['como_llegar'][0]),
                               "description" => wp_strip_all_tags($meta['descripcion'][0], true),
                               "to_do" => wp_strip_all_tags($meta['que_hacer'][0], true),
                               "best_season" => wp_strip_all_tags($meta['mejor_temporada'][0], true),
                               "province" => $parent_province,
                               "iframe" => $meta['como_llegar'][0],
                               "ig_images" => $ig_images,
                               "weather" => $meta['clima'][0],
                               "link" => $link,
                               "video" => $meta['video'],
                               "location" => $meta['location'][0] === null ? '' : $meta['location'],
                               "tourist_facilities" => wp_strip_all_tags($meta['facilidades_turisticas'][0], true),
                               "useful_info" => wp_strip_all_tags($meta['informacion_util'][0], true),
                               "latitude" => $location[1][0] === null ? '' : $location[1][0],
                               "longitude" => $location[2][0] === null ? '' : $location[2][0]
                ];
            }
        }

        return $response;
    }



    public function format_url($url = null)
    {
        $re = '/@(\-?[0-9]+\.[0-9]+),(\-?[0-9]+\.[0-9]+)/';
        preg_match($re, $url, $matches, PREG_OFFSET_CAPTURE, 0);
        // Print the entire match result
        return $matches;
    }

    public function extract_url($iframe = null)
    {
        preg_match('/src="([^"]+)"/', $iframe, $match);
        $url = $match[0];
        return $url;
    }
}
