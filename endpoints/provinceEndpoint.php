<?php

class provinceEndpoint extends WP_REST_Controller
{
    public function get_provinces($request = null)
    {
        $provinceEndpoint = new provinceEndpoint();
        $args = [
            "category_name" => "provincia",
            "post_status" => "publish",
            "post_type" => "page",
            "ICL_LANGUAGE_CODE" => "en",
            "posts_per_page" => -1
        ];
        $array_result = [];
        $result = get_posts($args);
        if ($request['language'] !== 'en') {
            foreach ($result as $element) {
                $array_result [] = $element;
            }
            $response = $provinceEndpoint->format_response($result);
            return $response;
        }
        return $provinceEndpoint->get_english_provinces($result);
    }

    public function get_english_provinces($posts = null)
    {
        global $wpdb;
        $result = [];
        $response = [];
        $ig_endpoints = new InstagramEndpoints();
        $args = [
            'post_type' => 'attachment',
            'post_mime_type' => 'image',
            'post_parent' => $request['id'],
        ];
        if ($posts !== null) {
            $query = "SELECT element_id from wp_icl_translations where language_code = 'en' AND trid = ";
            foreach ($posts as $elements) {
                $query_result[] = $wpdb->get_results($query.$elements->ID);
            }
            if (!empty($query_result)) {
                $images_array = [];
                foreach ($query_result as $value) {
                    $query = get_post($value[0]->element_id);
                    $thumbnail = get_the_post_thumbnail_url($value[0]->element_id);
                    $terms = get_term_by('slug', $query->post_name, 'category');
                    $meta = get_post_meta($value[0]->element_id);
                    $link = get_permalink($value[0]->element_id);
                    $response [] = ["id" => $query->ID, "title" => $query->post_title,
                                    "thumbnail" => $thumbnail,
                                    "term_id" => $terms->term_id, "post_name" => $query->post_name,
                                    "best_season" => $meta['best_season'], "ideal_for" => $meta['ideal_for'],
                                    "max_height" => $meta['max_height'], "province_description" => $meta['province_description'],
                                    "weather" => $meta['weather'],
                                    "link" => $link
                    ];
                }
            }
        }
        return $response;
    }

    public function get_province($request = null)
    {
        $provinceEndpoint = new provinceEndpoint();
        $ig_endpoints = new InstagramEndpoints();
        $args = [
            'post_type' => 'attachment',
            'post_mime_type' => 'image',
            'post_parent' => $request['id'],
        ];

        $images_array = [];
        $query = get_post($request['id']);
        $thumbnail = get_the_post_thumbnail_url($request['id']);
        $terms = get_term_by('slug', $query->post_name, 'category');
        $meta = get_post_meta($request['id']);
        $child_args = [
            'post_type' => 'destino',
            'category__and' => array($terms->term_id),
            'posts_per_page'=> '-1'
        ];
        $childs = get_posts($child_args);
        $post_images = get_posts($args);
        $slugs = $provinceEndpoint->get_activities_slug($childs);
        foreach ($post_images as $element) {
            $images_array[] = $element->guid;
        }

        $child_meta = $provinceEndpoint->get_child_meta($childs);
        $link = get_permalink($request['id']);
        $location = $provinceEndpoint->format_url($meta['location'][0]);
        $result_array = ["id" => $query->ID, "title" => $query->post_title, "images" => $images_array,
                         "thumbnail" => $thumbnail,
                         "term_id" => $terms->term_id, "taxonomy" => $terms->taxonomy,
                         "slug" => $terms->slug,
                         "best_season" => $meta['best_season'], "ideal_for" => $meta['ideal_for'],
                         "max_height" => $meta['max_height'], "province_description" => $meta['province_description'],
                         "weather" => $meta['weather'], "activities" => $slugs, "video" => $meta['video'],
                         "tour_operator" => $meta['tour_operadores'][0],
                         "travel" => $meta['viaje'][0],
                         "lodging" => $meta['alojamiento'][0],
                         "arrive" => $meta['arrive'],
                         "link" => $link,
                         "ig_images" => $ig_endpoints->get_instagram_feed_images($request['tag'], $request['count']),
                         "location" => $meta['location'][0] === null ? '' : $meta['location'],
                         "latitude" => $location[1][0] === null ? '' : $location[1][0],
                         "longitude" => $location[2][0] === null ? '' : $location[2][0]
        ];
        return ["province" => $result_array, "attractiveness" => $child_meta];
    }

    public function get_child_meta($child = null)
    {
        $result = [];
        $image_args = [
            'post_type' => 'attachment',
            'post_mime_type' => 'image',
            'post_parent' => null
        ];
        foreach ($child as $element) {
            $meta = get_post_meta($element->ID);
            $image_args['post_parent'] = $element->ID;
            $image = get_posts($image_args);
            $link = get_permalink($element->ID);
            $thumbnail = get_the_post_thumbnail_url($element->ID);
            $result[] =[
                    'id' => $element->ID,
                    'content' =>  wp_strip_all_tags($element->post_content, true),
                    'title' => $element->post_title,
                    'post_name' => $element->post_name, 'post_type' => $element->post_type,
                    'description' => wp_strip_all_tags($meta['descripcion'][0], true) ,
                    'arrive' => $meta['como_llegar'][0],
                    'to_do' => wp_strip_all_tags($meta['que_hacer'][0], true),
                    'link' => $link,
                    'best_season' => wp_strip_all_tags($meta['mejor_temporada'][0]), 'thumbnail' => $thumbnail
            ];
        }
        return $result;
    }

    public function format_response($result = null, $tag = null, $count = null)
    {
        $ig_endpoints = new InstagramEndpoints();
        $provinceEndpoint = new provinceEndpoint();
        $response = [];
        foreach ($result as $element) {
            $meta = get_post_meta($element->ID);
            $thumbnail = get_the_post_thumbnail_url($element->ID);
            $terms = get_term_by('slug', $element->post_name, 'category');
            $link = get_permalink($element->ID);
            $location = $provinceEndpoint->format_url($meta['location'][0]);
            $response [] = ["id" => $element->ID, "title" => $element->post_title,
                            "thumbnail" => $thumbnail,
                            "term_id" => $terms->term_id, "post_name" => $element->post_name,
                            "best_season" => $meta['best_season'], "ideal_for" => $meta['ideal_for'],
                            "max_height" => $meta['max_height'], "province_description" => $meta['province_description'],
                            "weather" => $meta['weather'],
                            "link" => $link,
                            "location" => $meta['location'][0] === null ? '' : $meta['location'],
                            "latitude" => $location[1][0] === null ? '' : $location[1][0],
                            "longitude" => $location[2][0] === null ? '' : $location[2][0]
            ];
        }
        return $response;
    }

    public function get_activities_slug($attractiveness = null)
    {
        $activities_id = get_term_children(67, 'category');
        $activitie_slug = [];
        $activities_result = [];
        foreach ($activities_id as $item) {
            $activitie_slug[] = get_term($item, 'category')->slug;
        }
        foreach ($attractiveness as $element) {
            $attractiveness_slug = get_the_category($element->ID);
            foreach ($attractiveness_slug as $_element) {
                if (in_array($_element->slug, $activitie_slug)) {
                    $activities_result[] = ["activitie_id" => $_element->term_id, "activitie" => $_element->name,
                                            "post_name" => $_element->slug];
                }
            }
        }

        $result_activities = array_unique($activities_result, SORT_REGULAR);
        foreach ($result_activities as $value) {
            $result [] = $value;
        }

        return  $result;
    }

    public function format_url($url = null)
    {
        $re = '/@(\-?[0-9]+\.[0-9]+),(\-?[0-9]+\.[0-9]+)/';
        preg_match($re, $url, $matches, PREG_OFFSET_CAPTURE, 0);
        // Print the entire match result
        return $matches;
    }
}
