<?php

class contactFormEndpoints
{
    public function post_form($request = null)
    {
        global $wpdb;
        global $phpmailer;
        $meta = GFFormsModel::get_form_meta(13);
        $contactFormEndpoint = new contactFormEndpoints();
        $notifications = $meta['notifications'];
        $timestamp = $_SERVER['REQUEST_TIME'];
        $date = new DateTime();
        $date->setTimestamp($timestamp);
        $date = $date->format('Y-m-d H:i:s');
        $request_scheme = $_SERVER['REQUEST_SCHEME'];
        $source_url = $request_scheme.'://'.$_SERVER['HTTP_HOST'].'contactanos/';
        $user_agent = $_SERVER['HTTP_USER_AGENT'];
        $entryArray =    array(
            'form_id' => $meta['id'],
            'date_created' => $date,
            'is_starred' => 0,
            'is_read' => 0,
            'ip'=> $_SERVER['REMOTE_ADDR'],
            'source_url' => $source_url,
            'user_agent' => $user_agent,
            'currency' => 'USD',
            'created_by' => 2,
            'status' => 'active'
        );
        if (!empty($request)) {
            if ($request['nombre'] && $request['correo'] && $request['pais']) {
                $result = $wpdb->insert(('wp_rg_lead'), $entryArray);
                $lead_id = $wpdb->insert_id;

                if ($result) {
                    foreach ($meta['fields'] as $value) {
                        if ($request[$value->adminLabel] !== null) {
                            $wpdb->insert(
                                ('wp_rg_lead_detail'),
                                array(
                                    'form_id' => $meta['id'],
                                    'lead_id' => $lead_id,
                                    'field_number' => $value->id,
                                    'value' => $request[$value->adminLabel]
                                )
                            );
                        }
                    }

                    $data = $contactFormEndpoint->getNotificationData($notifications);

                    $contactFormEndpoint->sendFormEmail($data, $request);

                    return ['success' => 200];
                }
            }

            return ['failure' => 400];
        }

        return ['failure' => 400];
    }

    /**
     * Send form Mail
     *
     * @param array   $data    payload
     * @param Request $request request
     *
     * @return bool
    */
    public function sendFormEmail($data, $request)
    {
        $phpmailer = new PHPMailer(true);
        $phpmailer->isSMTP();
        $phpmailer->Host       = 'smtp.gmail.com';
        $phpmailer->SMTPAuth   = true;
        $phpmailer->Username   = 'app.visitpanama@gmail.com';
        $phpmailer->Password   = '!ellugar9?tudestino!';
        $phpmailer->SMTPSecure = 'tls';
        $phpmailer->Port       = 587;
        $phpmailer->setFrom('app.visitpanama@gmail.com', $data['name']);
        $phpmailer->addAddress('bberrio@rootstack.com');
        $phpmailer->isHTML(true);
        $body = '<div style="display:flex, flex-direction:row">
		<div style="margin-top: 10px">
		  <label>'.$request['nombre'].'</label>
		</div>
		<div style="margin-top: 10px">
		  <label>'.$request['correo'].'</label>
		</div>
		<div style="margin-top:10px">
		  <label>'.$request['pais'].'</label>
		</div>
		<div style="margin-top:10px">
		  <label>'.$request['descripcion'].'</label>
		</div>
	  </div>';
        $phpmailer->Subject = 'Nueva Entrada del formulario de contactanos';
        $phpmailer->Body = $body;

        return $phpmailer->send();
    }

    public function getNotificationData($notificationArray)
    {
        $data = [];
        if (!empty($notificationArray)) {
            foreach ($notificationArray as $element) {
                if ($element['event'] === 'form_submission') {
                    $data = $element;
                }
            }

            return $data;
        }

        return $data;
    }
}
